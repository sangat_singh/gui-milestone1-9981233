﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_wf
{
    public partial class Form1 : Form
    {
        int score = 0;
        int x;
        System.Random r;
        public Form1()
        {
            InitializeComponent();

            r = new Random();
            x = r.Next(1, 5);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int attempts = Convert.ToInt32(label2.Text);
            if (attempts < 5)
            {
                label4.Text = " ";

                int n = Convert.ToInt32(comboBox1.Text);

                if (n.Equals(x))
                {
                    label4.Text = "Correct Guess!";
                    x = r.Next(1, 5);
                    score++;
                }
                attempts++;
                label2.Text = attempts.ToString();
            }

        }

        private void label2_TextChanged(object sender, EventArgs e)
        {
            if (label2.Text.Equals("5"))
            {
                label5.Text = "Score : " + score;
            }
        }
    }
}
