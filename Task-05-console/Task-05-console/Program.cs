﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Guess Mini Game (1-5) with in 5 attempts.\n");

            int attempts = 0;
            System.Random r = new Random();
            int x = r.Next(1,5);

            int score = 0;
            while (attempts < 5)
            {
                Console.Write("ATTEMPT " + ++attempts + ": Enter your number: ");
                int n = Convert.ToInt32(Console.ReadLine());
                if (n.Equals(x))
                {
                    Console.WriteLine("Correct Guess!");
                    x = r.Next(1,5);
                    score++;
                }
            }
            
                Console.WriteLine("\nScore : " + score);
            
            Console.ReadKey();
        }
    }
}
