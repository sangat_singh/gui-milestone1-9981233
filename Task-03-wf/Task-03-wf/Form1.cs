﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //label1.Text = comboBox1.Text;
            switch (comboBox1.Text)
            {
                case "Nokia":
                    {
                        label1.Text = "Nokia Lumia";
                        break;
                    }
                case "Apple":
                    {
                        label1.Text = "iPhone";
                        break;
                    }
                case "Blackberry":
                    {
                        label1.Text = "Blackberry Messenger";
                        break;
                    }
                case "Vodafone":
                    {
                        label1.Text = "Vodafone carrier";
                        break;
                    }
            }
        }
    }
}
