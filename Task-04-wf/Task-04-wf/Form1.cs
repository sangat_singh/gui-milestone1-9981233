﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> fruitsAndVeg = new Dictionary<string, string>();
            fruitsAndVeg.Add("apple", "fruit");
            fruitsAndVeg.Add("banana", "fruit");
            fruitsAndVeg.Add("onion", "vegetable");
            fruitsAndVeg.Add("cucumber", "vegetable");
            fruitsAndVeg.Add("corn", "vegetable");

            foreach (KeyValuePair<string, string> pair in fruitsAndVeg)
            {
                listBox1.Items.Add(pair.Key.ToString());
            }


            //listBox1.Items.Add("apple");
            //listBox1.Items.Add("banana");
            //listBox1.Items.Add("onion");
            //listBox1.Items.Add("cucumber");

            listBox1.Sorted = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int x = 0;
            
            if(textBox1.Text.Length != 0)
            {
                    x = listBox1.FindString(textBox1.Text, x);
                    if(x != -1)
                    {
                        listBox1.TopIndex = x;
                        listBox1.SetSelected(x, true);
                    if (listBox1.SelectedItem.ToString().Equals(textBox1.Text))
                        button1.Text = "Exists";
                    }
                    else
                    {
                        listBox1.SetSelected(0, false);
                    button1.Text = "Not Exists";
                }
            }
           
        }
    }
}
