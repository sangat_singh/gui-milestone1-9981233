﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_console
{
    class Program
    {
        static void Main(string[] args)
        {
            double total;
            Console.WriteLine("Enter First Number : ");
            double firstNum = Convert.ToDouble(Console.ReadLine());


            Console.WriteLine("Enter Second Number : ");
            double secondNum = Convert.ToDouble(Console.ReadLine());

            total = firstNum + secondNum;
            double gst = total * 1.15;
            Console.WriteLine("Total : "+ gst );

            Console.ReadLine();
        }
    }
}
