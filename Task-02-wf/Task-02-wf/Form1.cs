﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double total;
            double firstNnum = double.Parse(textBox1.Text);
            double secondNum = double.Parse(textBox2.Text);
            total = firstNnum + secondNum;
            double gst = total * 1.15;
            textBox3.Text = gst.ToString();
        }
    }
}
