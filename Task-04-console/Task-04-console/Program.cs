﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> fruitsAndVeg = new Dictionary<string, string>();
            fruitsAndVeg.Add("apple", "fruit");
            fruitsAndVeg.Add("banana", "fruit");
            fruitsAndVeg.Add("onion", "vegetable");
            fruitsAndVeg.Add("cucumber", "vegetable");

            foreach (KeyValuePair<string, string> pair in fruitsAndVeg)
            {
                Console.WriteLine(pair.Key.ToString() + "  -  " + pair.Value.ToString());
            }

            Console.Write("Enter a Value : ");
            string input = Console.ReadLine();

            bool result = false;
            foreach (KeyValuePair<string,string> pair in fruitsAndVeg)
            {
                if(pair.Key.Equals(input) || pair.Key.Equals(input.ToLower()) )
                {
                    result = true;
                }
            }
            Console.WriteLine(result?"Item is present":"Item is not present");
            Console.ReadKey();
        }



    }
    }


