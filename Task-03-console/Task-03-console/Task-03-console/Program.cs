﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Select : ");
            Console.WriteLine(" 1. Nokia \n 2. Apple \n 3. Blackberry \n 4. Vodafone");

            int option = Convert.ToInt32(Console.ReadLine());
            switch (option)
            {
                case 1:
                    {
                        Console.WriteLine("Nokia Lumia");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("iPhone");
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Blackberry Messenger");
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Vodafone Network");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Wrong Selection");
                        break;
                    }
            }

            Console.ReadLine();
        }
    }
}
