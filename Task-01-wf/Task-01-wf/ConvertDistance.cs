﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_wf
{
    public static class ConvertDistance
    {
        public static double ConvertMilesToKilometers(double miles)
        {
            return miles * 1.609344;
        }
        public static double ConvertKilometersToMiles(double kilometers)
        {
            return kilometers * 0.62137119;
        }
    }
}
