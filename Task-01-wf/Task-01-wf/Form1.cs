﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Converter : Form
    {
        public Converter()
        {
            InitializeComponent();
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            double distance, convertedDistance;
            if(double.TryParse(txtDistance.Text, out distance))
            {
                if(lstFrom.SelectedIndex != -1)
                {
                        if (lstFrom.SelectedIndex == 0 )
                        {
                        convertedDistance = ConvertDistance.ConvertKilometersToMiles(distance);    
                            lblConvertedDistance.Text = convertedDistance.ToString();
                        }
                        else
                        {
                        convertedDistance = ConvertDistance.ConvertMilesToKilometers(distance);
                            lblConvertedDistance.Text = convertedDistance.ToString();
                        }
                }
                else
                {
                    MessageBox.Show("Must Select Item: From");
                }
            }
            else
            {
                MessageBox.Show("Please Enter a Number For Distance");
            }
        }

    }
}
