﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            //User Input 
            Console.WriteLine("\n Select : \n 1. Convert KM to MILES");
            Console.WriteLine(" 2. Convert MILES to KM");

            int option = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter : ");
            double input = Convert.ToDouble(Console.ReadLine());
            switch (option)
            {
                case 1:
                    {
                        Console.WriteLine("Miles : " + ConvertDistance.ConvertKilometersToMiles(input));
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("KM : " + ConvertDistance.ConvertMilesToKilometers(input));
                        break;
                    }
            }
            Console.ReadLine();
        }
    }
}
